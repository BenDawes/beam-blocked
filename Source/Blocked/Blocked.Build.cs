// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Blocked : ModuleRules
{
	public Blocked(ReadOnlyTargetRules Target) : base(Target)
	{
        bUseUnity = false;
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "EnhancedInput", "GeometryScriptingCore", "GeometryFramework" });
	}
}
