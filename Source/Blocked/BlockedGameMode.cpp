// Copyright Epic Games, Inc. All Rights Reserved.

#include "BlockedGameMode.h"
#include "BlockedCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABlockedGameMode::ABlockedGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
