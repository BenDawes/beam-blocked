// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BlockedGameMode.generated.h"

UCLASS(minimalapi)
class ABlockedGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABlockedGameMode();
};



